# Movie Rating Service

### Application Information

- **Scope**: What rating particular user given to particular Move.
- Deals with
  - **Input**	:	UserId [movie ].
  - **Output**   :   MovieId with Movie ratings.





### Generic Info

- Deployed at port 9003