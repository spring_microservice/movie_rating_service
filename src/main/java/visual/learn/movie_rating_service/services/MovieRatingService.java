package visual.learn.movie_rating_service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import visual.learn.movie_rating_service.dto.RatingRepository;
import visual.learn.movie_rating_service.models.Rating;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Service
public class MovieRatingService {


    @Autowired
    private RatingRepository ratingRepository;


    @PostConstruct
    public void addDefaultStudents() {
        /*ratingRepository.saveAll(
                Arrays.asList(
                        new Rating("aa11",8),
                        (new Rating("bb22",9)
                        )));*/

        this.addMovieRating(new Rating("aa11",8));
        this.addMovieRating(new Rating("bb22",9));
    }

    public void addMovieRating(Rating rating){
        ratingRepository.save(rating);
    }

    public Rating getRatingOnMovieId(String movieId){
        return ratingRepository.findById(movieId).isPresent()
                ?ratingRepository.findById(movieId).get()
                :null;
    }

    public List<Rating> getMovieRatings() {
        return (List<Rating>) ratingRepository.findAll();
    }
}
