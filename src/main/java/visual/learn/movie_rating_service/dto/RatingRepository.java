package visual.learn.movie_rating_service.dto;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import visual.learn.movie_rating_service.models.Rating;

import java.util.Optional;

@Repository
public interface RatingRepository extends CrudRepository<Rating,String> {
    @Override
    <S extends Rating> S save(S s);

    @Override
    <S extends Rating> Iterable<S> saveAll(Iterable<S> iterable);

    @Override
    Iterable<Rating> findAll();

    @Override
    Iterable<Rating> findAllById(Iterable<String> iterable);

    @Override
    long count();
}
