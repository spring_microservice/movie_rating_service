package visual.learn.movie_rating_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@EnableEurekaClient
@ComponentScan(basePackages = {"visual.learn.movie_rating_service.services",
								"visual.learn.movie_rating_service.controllers",
								"visual.learn.movie_rating_service.models",
								"visual.learn.movie_rating_service.dto"})
@EntityScan("visual.learn.movie_rating_service.models")
@SpringBootApplication
public class MovieRatingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieRatingServiceApplication.class, args);
	}

}
