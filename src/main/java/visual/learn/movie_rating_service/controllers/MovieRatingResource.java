package visual.learn.movie_rating_service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import visual.learn.movie_rating_service.models.Rating;
import visual.learn.movie_rating_service.models.Ratings;
import visual.learn.movie_rating_service.services.MovieRatingService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/rating")
public class MovieRatingResource {


    @Autowired
    private MovieRatingService ratingService;


    @RequestMapping("/{movieId}")
    public Rating getRating(@PathVariable("movieId") String movieId) {
        return ratingService.getRatingOnMovieId(movieId);
    }


    @RequestMapping("/moovies")
    public Ratings getRating() {
        Ratings ratings =new Ratings();

        ratings.setRatings(ratingService.getMovieRatings());
        return ratings;
    }

    @RequestMapping("/users/{userId}")
    public Ratings geUsertRating(@PathVariable("userId") String userId) {

        List<Rating> ratings= Arrays.asList(
                new Rating("aa11",8),
                new Rating("bb22",9)
        );

        Ratings userRating=new Ratings();
        userRating.setRatings(ratings);
        return userRating;
    }
}
